
public class StockProfitCalculator {
	
	
	  public int calculateMaxProfit(int[] prices) {
		  	
		    if (prices.length < 2) {
		        throw new IllegalArgumentException("Calculating a profit requires 2 prices minimum");
		    }

		    int minimumPrice = prices[0];
		    int maximumProfit = prices[1] - prices[0]; //first possible profit

		    // starting iteration at i=1 as we must purchase first
		    for (int i = 1; i < prices.length; i++) {
		        int currentPrice = prices[i];
		        int currentProfit = currentPrice - minimumPrice;
		        // updating maxProfit if it is possible for maxProfit > currentProfit
		        maximumProfit = Math.max(maximumProfit, currentProfit);
		        // updating minPrice to the current minimum
		        minimumPrice = Math.min(minimumPrice, currentPrice);
		    }
		   
		    return maximumProfit;
		    
		}

}
