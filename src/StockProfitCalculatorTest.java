import org.junit.Assert;

class StockProfitCalculatorTest {

	class Tests {
		
		@org.junit.jupiter.api.Test
		void noPrices() {
			int[] prices = {};
			StockProfitCalculator profit = new StockProfitCalculator();
			Assert.assertEquals(-1, profit.calculateMaxProfit(prices));
		}

		@org.junit.jupiter.api.Test
		void singlePrice() {
			int[] prices = { 7 };
			StockProfitCalculator profit = new StockProfitCalculator();
			Assert.assertEquals(-1, profit.calculateMaxProfit(prices));
		}
		
		@org.junit.jupiter.api.Test
		void negativeProfit() {
			int[] prices = { 77, 7 };
			StockProfitCalculator profit = new StockProfitCalculator();
			Assert.assertEquals(-70, profit.calculateMaxProfit(prices));
		}

		@org.junit.jupiter.api.Test
		void randomPrices() {
			int[] prices = { 11, 7, 3, 5, 77, 2, 6, 7, 1, 5 };
			StockProfitCalculator profit = new StockProfitCalculator();
			Assert.assertEquals(74, profit.calculateMaxProfit(prices));
		}

		@org.junit.jupiter.api.Test
		void increasingPrices() {
			int[] prices = { 0, 1, 2, 3, 4, 5 };
			StockProfitCalculator profit = new StockProfitCalculator();
			Assert.assertEquals(5, profit.calculateMaxProfit(prices));
		}
		
		@org.junit.jupiter.api.Test
		void fixedPrice() {
			int[] prices = { 7, 7, 7, 7 };
			StockProfitCalculator profit = new StockProfitCalculator();
			Assert.assertEquals(0, profit.calculateMaxProfit(prices));
		}

		@org.junit.jupiter.api.Test
		void decreasingPrice() {
			int[] prices = { 5, 4, 3, 2, 1, 0 };
			StockProfitCalculator profit = new StockProfitCalculator();
			Assert.assertEquals(-1, profit.calculateMaxProfit(prices));
		}

		@org.junit.jupiter.api.Test
		void duplicatePrices() {
			int[] prices = { 7, 7, 5, 5, 0, 0, 9, 9, 9, 13, 13, 13 };
			StockProfitCalculator profit = new StockProfitCalculator();
			Assert.assertEquals(13, profit.calculateMaxProfit(prices));
		}

	}


}
